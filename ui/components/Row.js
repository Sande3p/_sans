import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedRow = connectAnimation(View);
const Row = connectStyle('shoutem.ui.Row')(AnimatedRow);

export {
  Row,
};
