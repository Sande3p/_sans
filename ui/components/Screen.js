import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedScreen = connectAnimation(View);
const Screen = connectStyle('shoutem.ui.Screen')(AnimatedScreen);

export {
  Screen,
};
