import { default as Lightbox } from 'react-native-lightbox';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedLightbox = connectAnimation(Lightbox);
const StyledLightbox = connectStyle('shoutem.ui.Lightbox')(AnimatedLightbox);

export {
  StyledLightbox as Lightbox,
};
