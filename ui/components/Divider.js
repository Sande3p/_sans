import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedDivider = connectAnimation(View);
const Divider = connectStyle('shoutem.ui.Divider')(AnimatedDivider);

export {
  Divider,
};
