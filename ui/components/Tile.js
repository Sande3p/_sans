import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedTile = connectAnimation(View);
const Tile = connectStyle('shoutem.ui.Tile')(AnimatedTile);

export {
  Tile,
};
