import React, { Component } from 'react';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

import { View } from './View';

class FormGroup extends Component {
  render() {
    return (
      <View {...this.props}>
        {this.props.children}
      </View>
    );
  }
}

FormGroup.propTypes = {
  ...View.propTypes
};

const AnimatedFormGroup = connectAnimation(FormGroup);
const StyledFormGroup = connectStyle('shoutem.ui.FormGroup')(AnimatedFormGroup);

export {
  StyledFormGroup as FormGroup,
};
