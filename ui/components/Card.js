import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedCard = connectAnimation(View);
const Card = connectStyle('shoutem.ui.Card', {})(AnimatedCard);

export {
  Card,
};
