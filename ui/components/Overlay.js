import { View } from 'react-native';

import { connectStyle } from '@sans/theme';
import { connectAnimation } from '@sans/animation';

const AnimatedOverlay = connectAnimation(View);
const Overlay = connectStyle('shoutem.ui.Overlay')(AnimatedOverlay);

export {
  Overlay,
};
